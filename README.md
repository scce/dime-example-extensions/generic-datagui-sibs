# Data-GUI-SIBs extension for DIME

This example illustrates how to create a SIB that references a `Data` model that already is available in DIME.

At generation time, from such a Data-SIB a GUI model is generated and the GenericSIB is replaced by a GUISIB that references this generated GUI model.

## Setup the plugin project

Clone this repository to a folder that will be referred to as 'repo location' in the following.

The extension project is supposed to be imported into DIME.
Install DIME from an installer and run it with an empty workspace.

From the context menu of the Project Explorer select 'Import > Existing projects into workspace'.
In the opening dialog, enter your repo location as root directory.
Mark the checkbox of the `info.scce.dime.datagui.sib` project and hit 'Finish'.

The project should now have been imported into your workspace.

Notice the registered extension for the extension point `info.scce.dime.modeltrafo` in the plugin.xml file.

```
<extension point="info.scce.dime.modeltrafo">
	<modeltrafo modeltrafosupporter="info.scce.dime.datagui.sib.extensionProviders.DataGUISupporter">
	</modeltrafo>
</extension>
```

## Run the product

From the context menu of any of the projects in the Project Explorer select 'Run As > Eclipse Application' to start the product instance.
It can be seen as a new instance of DIME that in addition to the standard bundles contains the `info.scce.dime.datagui.sib` bundle from your workspace, which adds the Data-GUI-SIBs functionality.

## Setup the runtime project

Head over to the [generic-datagui-sibs-app](https://gitlab.com/scce/dime-example-apps/generic-datagui-sibs-app) project and follow the instructions there to setup the demo runtime project and tryout the Data-GUI-SIBs.