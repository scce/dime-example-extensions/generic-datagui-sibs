package info.scce.dime.datagui.sib.transformator

import info.scce.dime.api.modelgen.GUIModelGenerationLanguage
import info.scce.dime.data.data.Type
import info.scce.dime.gui.actions.ToggleAttributeExpand
import info.scce.dime.gui.factory.GUIFactory
import info.scce.dime.gui.gui.ComplexVariable
import info.scce.dime.gui.gui.GUI
import org.eclipse.core.runtime.IPath

import static info.scce.dime.gui.factory.GUIFactory.*

import static extension org.eclipse.emf.ecore.util.EcoreUtil.setID

class DataToGuiGenerator {

	extension GUIModelGenerationLanguage = new GUIModelGenerationLanguage

	val Type type
	GUI gui

	new(Type model) {
		this.type = model
	}

	static def GUI generateGUI(Type model, IPath outlet, String fileName, String modelId) {
		new DataToGuiGenerator(model).generateGUI(outlet, fileName, modelId)
	}

	def GUI generateGUI(IPath outlet, String fileName, String modelId) {
		val factory = GUIFactory.eINSTANCE
		try {
			GUIFactory.eINSTANCE = new GUIFactory
			_gUIModelGenerationLanguage = new GUIModelGenerationLanguage
			gui = createGUI(outlet, fileName)
			gui.transact("Generate gui model " + fileName) [
				if (!modelId.nullOrEmpty) {
					gui.setID(modelId)
				}
				gui.title = type.name + "OverviewGUI"
				gui.fillGUI
				gui.save;
			]
		} catch(Exception e) {
			throw new RuntimeException(e)
		} finally {
			GUIFactory.eINSTANCE = factory
		}
		return gui
	}
	
	def fillGUI (GUI it) {
		val int fieldHeight = 15;
		val context = dataContexts.head
		val variable = context.newComplexVariable(type, context.x+5, context.y+5) => [
			isList = false
			name = type.getVariableName
			isInput = false
			toggleExpand
		]
		body.rows.forEach[delete]
		val form = body.newForm(200,200)
		variable.newFormSubmit(form)
		var int y = 0;
		for (primitiveAttr : variable.primitiveAttributes) {
			val field = form.newField(0,y);
			field.label = primitiveAttr.attribute.name.toFirstUpper
			primitiveAttr.newFormSubmit(field);
			y = y-fieldHeight;
		}
		val button = form.newButton(0, y)
		button.displayLabel = "Create"
		button.label = type.getButtonLabel
		form.buttons.findFirst[label.equals("Submit")].delete
		
		variable.newAddComplexToSubmission(button)
	}
	
	def getBody(GUI gui){
		gui.templates.filter[blockName == "body"].head
	}
	
	def toggleExpand(ComplexVariable complexVar) {
		new ToggleAttributeExpand().execute(complexVar)
	}
	
	def static String getButtonLabel(Type type) {
		return "Add " + type.name
	}
	
	def static String getVariableName(Type type) {
		return "new" + type.name
	}
}
