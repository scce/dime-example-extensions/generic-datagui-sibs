package info.scce.dime.datagui.sib.transformationProviders

import info.scce.dime.datagui.sib.transformator.DataToGuiGenerator
import info.scce.dime.api.modelgen.GUIModelGenerationLanguage
import info.scce.dime.data.data.Type
import info.scce.dime.gui.factory.GUIFactory
import info.scce.dime.gui.gui.GUI
import info.scce.dime.modeltrafo.extensionpoint.transformation.ISIBtoGUITransformer
import org.eclipse.core.resources.IContainer
import org.eclipse.core.resources.IFile
import org.eclipse.core.resources.IFolder
import org.eclipse.core.resources.IProject
import org.eclipse.core.resources.ResourcesPlugin
import org.eclipse.core.runtime.IPath
import org.eclipse.core.runtime.Path
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.Resource

import static extension info.scce.dime.generator.util.EclipseUtils.*

class DataToGUITransformator implements ISIBtoGUITransformer<Type> {
	
	protected extension GUIModelGenerationLanguage = new GUIModelGenerationLanguage
	protected var GUIFactory factory
	
	override GUI transform(Type referencedSIBModel) {
		var IFile file = getFile(referencedSIBModel.eResource)
		var IContainer parent = file.parent
		while (!(parent instanceof IProject)) {
			parent = parent.parent
		}
		var IPath outlet = parent.fullPath
		outlet = outlet.removeLastSegments(1).append("transform-gen")
		val IFolder outputFolder = parent.getFolder(outlet)
		outputFolder.mkdirs
		return DataToGuiGenerator.generateGUI(referencedSIBModel, outputFolder.fullPath, referencedSIBModel.fileName, null)
	}

	
	static protected def IFile getFile(Resource resource) {
		if (resource !== null) {
			var URI uri = resource.getURI();
			uri = resource.getResourceSet().getURIConverter().normalize(uri);
			var String scheme = uri.scheme();
			if ("platform".equals(scheme) && uri.segmentCount() > 1 && "resource".equals(uri.segment(0))) {
				var StringBuffer platformResourcePath = new StringBuffer();
				for (var int j = 1, var size = uri.segmentCount(); j < size; j++) {
					platformResourcePath.append('/');
					platformResourcePath.append(uri.segment(j));
				}
				return ResourcesPlugin.getWorkspace().getRoot().getFile(new Path(platformResourcePath.toString()));
			}
		}
		return null;
	}
	
	protected def String getFileName(Type model) {
		model.name
	}
	
	protected def String getLastSegment(String fqn) {
		var String[] segments = fqn.split("\\.");
		if (segments.length>0) {			
			return segments.get(segments.length-1);
		}
		return fqn;
	}
	
}
