package info.scce.dime.datagui.sib.extensionProviders;

import java.net.URL;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.ecore.EClass;
import org.osgi.framework.Bundle;

import info.scce.dime.datagui.sib.modellingProviders.DataGUISIBModellingProvider;
import info.scce.dime.datagui.sib.transformationProviders.DataToGUITransformator;
import info.scce.dime.data.data.DataPackage;
import info.scce.dime.data.data.Type;
import info.scce.dime.modeltrafo.extensionpoint.buildmodel.IGenericSIBBuilder;
import info.scce.dime.modeltrafo.extensionpoint.trafosupport.IModeltrafoSupporter;
import info.scce.dime.modeltrafo.extensionpoint.transformation.ISIBGenerationProvider;

public class DataGUISupporter implements IModeltrafoSupporter<Type> {
	
	private static DataGUISIBModellingProvider modellingProvider = new DataGUISIBModellingProvider();
	private static DataToGUITransformator generationProvider = new DataToGUITransformator();
	
	@Override
	public String getModelExtension() {
		return "data";
	}

	@Override
	public String getSIBName() {
		return "DataGUISIB";
	}

	@Override
	public String getIconPath() {
		Bundle bundle = Platform.getBundle("info.scce.dime.datagui.sib");
		URL url = FileLocator.find(bundle, new Path("icons/dataSIB.png"), null);
		try {
			URL fileURL = FileLocator.toFileURL(url);
			return fileURL.getFile();
		} catch (Exception e) {
			throw new IllegalStateException(e);
		}
	}

	@Override
	public IGenericSIBBuilder<Type> getModellingProvider() {
		return modellingProvider;
	}

	@Override
	public EClass getModelType() {
		return DataPackage.eINSTANCE.getType();
	}

	@Override
	public ISIBGenerationProvider getGenerationProvider() {
		return generationProvider;
	}

}
