package info.scce.dime.datagui.sib.modellingProviders;

import java.util.ArrayList;
import java.util.List;

import info.scce.dime.datagui.sib.transformator.DataToGuiGenerator;
import info.scce.dime.data.data.Type;
import info.scce.dime.modeltrafo.extensionpoint.buildmodel.GenericComplexPort;
import info.scce.dime.modeltrafo.extensionpoint.buildmodel.GenericOutputBranch;
import info.scce.dime.modeltrafo.extensionpoint.buildmodel.GenericPort;
import info.scce.dime.modeltrafo.extensionpoint.buildmodel.IGenericSIBBuilder;

public class DataGUISIBModellingProvider implements IGenericSIBBuilder<Type> {

	@Override
	public String getName(Type model) {
		return model.getName();
	}

	@Override
	public String getLabel(Type model) {
		return model.getName();
	}

	@Override
	public List<GenericPort> getInputPorts(Type model) {
		return new ArrayList<GenericPort>();
	}

	@Override
	public List<GenericOutputBranch> getOutputBranches(Type model) {
		List<GenericOutputBranch> outputBranches = new ArrayList<GenericOutputBranch>();
		List<GenericPort> outputPorts = new ArrayList<GenericPort>();
		outputPorts.add(new GenericComplexPort(DataToGuiGenerator.getVariableName(model), model));
		outputBranches.add(new GenericOutputBranch(DataToGuiGenerator.getButtonLabel(model), outputPorts));
		return outputBranches;
	}

}
